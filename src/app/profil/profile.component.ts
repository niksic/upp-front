import { Component, OnInit } from '@angular/core';
import {LoginModel} from "../models/login.model";
import {ServerService} from "../server.service";
import {Response} from "@angular/http";
import {Router} from "@angular/router";

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

  public tempUserInfo: LoginModel = new LoginModel('', '', '', '', '', '', '');

  constructor(private serverService: ServerService, private router: Router) {
    console.log('constructor');
    this.tempUserInfo.id = this.serverService.loggedUser.id;
    this.tempUserInfo.firstname = this.serverService.loggedUser.firstname;
    this.tempUserInfo.lastName = this.serverService.loggedUser.lastName;
    this.tempUserInfo.username = this.serverService.loggedUser.username;
    this.tempUserInfo.userPassword = this.serverService.loggedUser.userPassword;
    this.tempUserInfo.status = this.serverService.loggedUser.status;
    this.tempUserInfo.type = this.serverService.loggedUser.type;
    this.tempUserInfo.category = this.serverService.loggedUser.category;

    console.log(this.tempUserInfo);
  }

  ngOnInit() {
  }

  onCancel() {
    this.tempUserInfo.id = this.serverService.loggedUser.id;
    this.tempUserInfo.firstname = this.serverService.loggedUser.firstname;
    this.tempUserInfo.lastName = this.serverService.loggedUser.lastName;
    this.tempUserInfo.username = this.serverService.loggedUser.username;
    this.tempUserInfo.userPassword = this.serverService.loggedUser.userPassword;
    this.tempUserInfo.status = this.serverService.loggedUser.status;
    this.tempUserInfo.type = this.serverService.loggedUser.type;
    this.tempUserInfo.category = this.serverService.loggedUser.category;
    console.log(this.tempUserInfo);
  }

  onEdit() {
    this.serverService.putForEditProfile(this.tempUserInfo).subscribe(
      (response: Response) => {
        let loginModel: LoginModel = response.json();
        if (loginModel.status === 'VALID') {
          this.serverService.loggedUser = loginModel;
          toastr.success('Edit finished successfully');
        } else {
          toastr.error('Username is not unique!');
        }
        this.onCancel();
      }
    );
  }

  redirectToChangePassword() {
    this.router.navigateByUrl('/changePassword');
  }
}
