import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Params, Router} from "@angular/router";
import {LoginModel} from "../../../models/login.model";
import {ServerService} from "../../../server.service";
import {Response} from "@angular/http";
import {DataService} from "../../../data.service";

@Component({
  selector: 'app-user-detail',
  templateUrl: './user-detail.component.html',
  styleUrls: ['./user-detail.component.css']
})
export class UserDetailComponent implements OnInit {

  id: string;
  user: LoginModel = new LoginModel('', '', '', '', '', '', '');

  constructor(private route: ActivatedRoute, private serverService: ServerService,
              private dataService: DataService, private router: Router) { }

  ngOnInit() {
    this.serverService.getListOfCategories().subscribe(
      (response: Response) => {
        this.dataService.categories = [];
        let responseCategories = response.json();
        for (var i = responseCategories.length - 1; i >= 0; i--) {
          this.dataService.categories.push(responseCategories[i]);
        }
      });
    this.route.params.subscribe(
      (params: Params) => {
        this.id = params['id'];
        console.log(this.id);
        this.serverService.getUser(this.id).subscribe(
          (response: Response ) => {
            this.user = response.json();
          }
        );
      });
  }

  onEdit() {
    this.serverService.putForEditProfile(this.user).subscribe(
      (response: Response) => {
        let loginModel: LoginModel = response.json();
        if (loginModel.status === 'VALID') {
          toastr.success('Edit finished successfully');
        } else {
          toastr.error('Username is not unique!');
        }
        this.dataService.updateUser(this.user);
        this.onCancel();
      }
    );
  }

  onCancel() {
    this.serverService.getUser(this.id).subscribe(
      (response: Response ) => {
        this.user = response.json();
      }
    );
  }

  onDelete() {
    this.serverService.deleteUser(this.user.id).subscribe(
      (response: Response) => {
        this.dataService.deleteUser(this.user.id);
        toastr.success('User is deleted successfully!');
        this.router.navigateByUrl('/users');
      }, (error) => {
        console.log(error);
        toastr.error('Delete failed!');
      }
    );
  }

}
