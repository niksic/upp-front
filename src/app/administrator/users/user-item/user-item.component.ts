import {Component, Input, OnInit} from '@angular/core';
import {LoginModel} from "../../../models/login.model";

@Component({
  selector: 'app-user-item',
  templateUrl: './user-item.component.html',
  styleUrls: ['./user-item.component.css']
})
export class UserItemComponent implements OnInit {

  @Input() user: LoginModel;

  constructor() { }

  ngOnInit() {
  }

}
