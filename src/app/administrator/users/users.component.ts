import { Component, OnInit } from '@angular/core';
import {LoginModel} from "../../models/login.model";
import {ServerService} from "../../server.service";
import {Response} from "@angular/http";
import {DataService} from "../../data.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {

  constructor(private serverService: ServerService, private dataService: DataService, private router: Router) {

  }

  ngOnInit() {
    console.log('ON INIT');
    if (this.serverService.loggedUser !== undefined) {
      this.serverService.getListOfUsers().subscribe(
        (response: Response) => {
          console.log(response.json());
          let responseUsers = response.json();
          this.dataService.users = [];
          for (var i = responseUsers.length - 1; i >= 0; i--) {
            this.dataService.users.push(responseUsers[i]);
          }
        }
      );
    }
  }

  onAddNewUser() {
    this.router.navigateByUrl('/users/add');
  }

}
