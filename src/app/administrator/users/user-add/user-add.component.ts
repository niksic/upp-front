import { Component, OnInit } from '@angular/core';
import {LoginModel} from "../../../models/login.model";
import {ServerService} from "../../../server.service";
import {Response} from "@angular/http";
import {DataService} from "../../../data.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-user-add',
  templateUrl: './user-add.component.html',
  styleUrls: ['./user-add.component.css']
})
export class UserAddComponent implements OnInit {

  user: LoginModel = new LoginModel('', '', '', '', '', '', '');

  constructor(private serverService: ServerService, private dataService: DataService, private router: Router) { }

  ngOnInit() {
    this.onCancel();
    this.serverService.getListOfCategories().subscribe(
      (response: Response) => {
        this.dataService.categories = [];
        let responseCategories = response.json();
        for (var i = responseCategories.length - 1; i >= 0; i--) {
          this.dataService.categories.push(responseCategories[i]);
        }
      }
    );
  }

  addUser() {
    this.serverService.postUser(this.user).subscribe(
      (response: Response) => {
        this.user = response.json();
        toastr.success('User is successfully added!');
        this.dataService.users.push(this.user);
        this.onCancel();
        this.router.navigateByUrl('/users');
      } , (error) => {
        toastr.error('Adding new user failed!');
      }
    );
  }

  onCancel() {
    this.user = new LoginModel('', '', '', '', '', '', '');
  }


}
