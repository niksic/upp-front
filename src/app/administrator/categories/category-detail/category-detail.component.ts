import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Params, Router} from "@angular/router";
import {ServerService} from "../../../server.service";
import {CategoryModel} from "../../../models/category.model";
import {Response} from "@angular/http";
import {DataService} from "../../../data.service";

@Component({
  selector: 'app-category-detail',
  templateUrl: './category-detail.component.html',
  styleUrls: ['./category-detail.component.css']
})
export class CategoryDetailComponent implements OnInit {

  id: string;
  category: CategoryModel = new CategoryModel('');

  constructor(private route: ActivatedRoute, private serverService: ServerService,
              private router: Router, private dataService: DataService ) { }

  ngOnInit() {

    this.route.params.subscribe(
      (params: Params) => {
        this.id = params['id'];
        console.log(this.id);
        this.serverService.getCategory(this.id).subscribe(
          (response: Response) => {
            this.category = response.json();
          }
        );
      });
  }

  onCancel() {
    this.serverService.getCategory(this.id).subscribe(
      (response: Response ) => {
        this.category = response.json();
      }
    );
  }

  onEdit() {
    this.serverService.putForEditCategory(this.category).subscribe(
      (response: Response) => {
        let category: CategoryModel = response.json();
        toastr.success('Edit finished successfully');
        this.dataService.updateCategory(category);
        this.onCancel();
      }, (error) => {
        toastr.error('Edit category failed!');
      }
    );
  }

  onDelete() {
    this.serverService.deleteCategory(this.category.id).subscribe(
      (response: Response) => {
        this.dataService.deleteCategory(this.category.id);
        toastr.success('Category is deleted successfully!');
        this.router.navigateByUrl('/categories');
      }, (error) => {
        toastr.error('Delete category failed!');
      }
    );
  }

}
