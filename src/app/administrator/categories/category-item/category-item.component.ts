import {Component, Input, OnInit} from '@angular/core';
import {CategoryModel} from "../../../models/category.model";

@Component({
  selector: 'app-category-item',
  templateUrl: './category-item.component.html',
  styleUrls: ['./category-item.component.css']
})
export class CategoryItemComponent implements OnInit {

  @Input() category: CategoryModel;

  constructor() { }

  ngOnInit() {
  }

}
