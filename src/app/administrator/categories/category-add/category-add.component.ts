import { Component, OnInit } from '@angular/core';
import {CategoryModel} from "../../../models/category.model";
import {ServerService} from "../../../server.service";
import {Response} from "@angular/http";
import {DataService} from "../../../data.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-category-add',
  templateUrl: './category-add.component.html',
  styleUrls: ['./category-add.component.css']
})
export class CategoryAddComponent implements OnInit {

  public category: CategoryModel = new CategoryModel('');

  constructor(private router: Router, private serverService: ServerService, private dataService: DataService) { }

  ngOnInit() {

    this.onCancel();

  }

  addCategory() {
    this.serverService.postCategory(this.category).subscribe(
      (response: Response) => {
        this.category = response.json();
        toastr.success('Category is successfully added!');
        this.dataService.categories.push(this.category);
        this.onCancel();
        this.router.navigateByUrl('/categories');
      } , (error) => {
        toastr.error('Adding new category failed!');
      }
    );
  }

  onCancel() {
    this.category = new CategoryModel ('');
  }


}
