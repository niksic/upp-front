import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";
import {DataService} from "../../data.service";
import {ServerService} from "../../server.service";
import {Response} from "@angular/http";

@Component({
  selector: 'app-categories',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.css']
})
export class CategoriesComponent implements OnInit {

  constructor(private router: Router, private dataService: DataService, private serverService: ServerService) { }

  ngOnInit() {
    console.log('ON INIT');
    if (this.serverService.loggedUser !== undefined) {
      this.serverService.getListOfCategories().subscribe(
        (response: Response) => {
          console.log(response.json());
          let responseCategories = response.json();
          this.dataService.categories = [];
          for (var i = responseCategories.length - 1; i >= 0; i--) {
            this.dataService.categories.push(responseCategories[i]);
          }
        }
      );
    }
  }

  onAddNewCategory() {
    this.router.navigateByUrl('/categories/add');
  }
}
