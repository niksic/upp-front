import { Component, OnInit } from '@angular/core';
import {EBooksModel} from "../../../models/eBooks.model";
import {DataService} from "../../../data.service";
import {ServerService} from "../../../server.service";
import {ActivatedRoute, Params, Router} from "@angular/router";
import {Response} from "@angular/http";

@Component({
  selector: 'app-e-book-detail',
  templateUrl: './e-book-detail.component.html',
  styleUrls: ['./e-book-detail.component.css']
})
export class EBookDetailComponent implements OnInit {

  id: string;
  book: EBooksModel = new EBooksModel( '', '', '', null, '', '');
  file: File;
  downloadString: String;

  constructor(private dataService: DataService, private serverService: ServerService,
              private router: Router, private route: ActivatedRoute) { }

  ngOnInit() {
    this.route.params.subscribe(
      (params: Params) => {
        this.id = params['id'];
        console.log(this.id);
        this.serverService.getBook(this.id).subscribe(
          (response: Response ) => {
            this.book = response.json();
            var arr: String [];
            arr = this.book.fileName.split('\\');
            this.downloadString = 'http://localhost:8080/' + arr[arr.length - 1];
            console.log('DOWNLOAD' + this.downloadString);
            this.onCancel();
          }
        );
      });

  }

  onEdit() {
    this.serverService.putForUpdateBook(this.book).subscribe(
      (response: Response) => {
        this.book = response.json();
        toastr.success('Book updated successfully!');
        this.dataService.updateBook(this.book);
        this.router.navigateByUrl('/eBooks');
      }, (error) => {
        toastr.error('Update book failed!');
        this.onCancel();
      }
    );
  }

  onCancel () {
    this.serverService.getBook(this.id).subscribe(
      (response: Response ) => {
        this.book = response.json();
      }
    );
  }

  onDelete () {
    this.serverService.deleteBook(this.book.id).subscribe(
      (response: Response) => {
        this.dataService.deleteBook(this.book.id);
        toastr.success('Book is deleted successfully!');
        this.router.navigateByUrl('/eBooks');
      }, (error) => {
        toastr.error('Delete failed!');
      }
    );
  }

  setCategory() {
    console.log('CHANGE CATEGORY');
    console.log(this.book);
    for (var i = this.dataService.categories.length - 1; i >= 0; i--) {
      if (this.dataService.categories[i].name === this.book.category.name) {
        this.book.category.id = this.dataService.categories[i].id;
        console.log('SET ID ' + this.book.category.id);
        break;
      }
    }
  }

  setLanguage() {
    console.log('CHANGE LANGUAGE');
    for (var i = this.dataService.languages.length - 1; i >= 0; i--) {
      if (this.dataService.languages[i].name === this.book.language.name) {
        this.book.language.id = this.dataService.languages[i].id;
        console.log('SET ID ' + this.book.language.id);
        break;
      }
    }
  }

  fileEvent(event) {
    this.file = event.target.files[0];
  }

  checkUpload() {
    return this.file === undefined;
  }

  uploadEbook(){
    console.log(this.file);
    this.serverService.uploadFile(this.file).subscribe(
      (response: Response) => {
        toastr.success('Upload done successfully!');
        const responseEbook: EBooksModel = response.json();
        this.book.fileName = responseEbook.fileName;
        this.book.author = responseEbook.author;
        this.book.title = responseEbook.title;
        this.book.keywords = responseEbook.keywords;
        this.book.mime = responseEbook.mime;
      }, (error) => {
        toastr.error('Upload book failed!');
        this.onCancel();
      }
    );
  }


}
