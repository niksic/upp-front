import { Component, OnInit } from '@angular/core';
import {EBooksModel} from "../../../models/eBooks.model";
import {Router} from "@angular/router";
import {DataService} from "../../../data.service";
import {ServerService} from "../../../server.service";
import {Response} from "@angular/http";

@Component({
  selector: 'app-e-book-add',
  templateUrl: './e-book-add.component.html',
  styleUrls: ['./e-book-add.component.css']
})
export class EBookAddComponent implements OnInit {

  book: EBooksModel = new EBooksModel( '', '', '', null, '', '');
  file: File;

  constructor(private router: Router, private dataService: DataService, private serverService: ServerService) { }

  ngOnInit() {
    this.serverService.getLanguages().subscribe(
      (response: Response) => {
        let responseLanguage = response.json();
        this.dataService.languages = [];
        for (var i = responseLanguage.length - 1; i >= 0; i--) {
          this.dataService.languages.push(responseLanguage[i]);
        }
      }, (error) => {
        toastr.error('GET LANGUAGES FAILED');
      }
    );
  }

  addEBook() {
    console.log(this.book);
    this.serverService.postBook(this.book).subscribe(
      (response: Response) => {
        this.book = response.json();
        toastr.success('Book is successfully added!');
        this.dataService.books.push(this.book);
        this.onCancel();
        this.router.navigateByUrl('/eBooks');
      }, (error) => {
        toastr.success('Adding book failed!');
        this.onCancel();
      }
    );
  }

  setCategory() {
    console.log('CHANGE CATEGORY');
    console.log(this.book);
    for (var i = this.dataService.categories.length - 1; i >= 0; i--) {
      if (this.dataService.categories[i].name === this.book.category.name) {
        this.book.category.id = this.dataService.categories[i].id;
        console.log('SET ID ' + this.book.category.id);
        break;
      }
    }
  }

  onCancel() {
    this.book = new EBooksModel( '', '', '', 0, '', '');
  }

  setLanguage() {
    console.log('CHANGE LANGUAGE');
    for (var i = this.dataService.languages.length - 1; i >= 0; i--) {
      if (this.dataService.languages[i].name === this.book.language.name) {
        this.book.language.id = this.dataService.languages[i].id;
        console.log('SET ID ' + this.book.language.id);
        break;
      }
    }
  }

  fileEvent(event) {
    this.file = event.target.files[0];
  }

  checkUpload() {
    return this.file === undefined;
  }

  uploadEbook() {
    console.log(this.file);
    this.serverService.uploadFile(this.file).subscribe(
      (response: Response) => {
        toastr.success('Upload done successfully!');
        const responseEbook: EBooksModel = response.json();
        this.book.fileName = responseEbook.fileName;
        this.book.author = responseEbook.author;
        this.book.title = responseEbook.title;
        this.book.keywords = responseEbook.keywords;
        this.book.mime = responseEbook.mime;
      }, (error) => {
        toastr.error('Upload book failed!');
        this.onCancel();
      }
    );
  }
}
