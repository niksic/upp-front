import {Component, Input, OnInit} from '@angular/core';
import {EBooksModel} from "../../../models/eBooks.model";
import {DataService} from "../../../data.service";

@Component({
  selector: 'app-e-book-item',
  templateUrl: './e-book-item.component.html',
  styleUrls: ['./e-book-item.component.css']
})
export class EBookItemComponent implements OnInit {

  @Input() book: EBooksModel;

  constructor(private dataService: DataService) { }

  ngOnInit() {
  }

}
