import { Component, OnInit } from '@angular/core';
import {CategoryModel} from "../../models/category.model";
import {DataService} from "../../data.service";
import {ServerService} from "../../server.service";
import {Response} from "@angular/http";
import {Router} from "@angular/router";

@Component({
  selector: 'app-e-books',
  templateUrl: './e-books.component.html',
  styleUrls: ['./e-books.component.css']
})
export class EBooksComponent implements OnInit {

  category: CategoryModel;

  constructor(private dataService: DataService, private serverService: ServerService, private router: Router) { }

  ngOnInit() {
    console.log('ON INIT');
      this.serverService.getListOfCategories().subscribe(
        (response: Response) => {
          console.log(response.json());
          let responseCategories = response.json();
          this.dataService.categories = [];
          for (var i = responseCategories.length - 1; i >= 0; i--) {
            this.dataService.categories.push(responseCategories[i]);
          }
        }
      );

    this.getAllBooks();
    console.log('BOOKS');
    console.log(this.dataService.books);
    this.serverService.getLanguages().subscribe(
      (response: Response) => {
        let responseLanguages = response.json();
        this.dataService.languages = [];
        for (var i = responseLanguages.length - 1; i >= 0; i--) {
          this.dataService.languages.push(responseLanguages[i]);
        }
      }, (error) => {
        toastr.error('LANGUAGES FAILED!');
      }
    );
  }

  onAddNewBook() {
    console.log('Add Book');
    this.router.navigateByUrl('/eBooks/add');
  }

  filterBooks() {
    console.log('FILTER');
    console.log(this.category);
    if (this.category.name === 'every') {
      this.getAllBooks();
    }
    this.getBooksByCategory();

  }

  private getBooksByCategory() {
    this.dataService.books = [];
    this.serverService.getBooksByCategory(this.category.id).subscribe(
      (response: Response) => {
        let responseBooks = response.json();
        console.log('CATEGORY BOOKS');
        console.log(response.json());
        for (var i = responseBooks.length - 1; i >= 0; i--) {
          this.dataService.books.push(responseBooks[i]);
        }
      }
    );
  }

  private getAllBooks() {
    this.dataService.books = [];
    this.serverService.getAllBooks().subscribe(
      (response: Response) => {
        let responseBooks = response.json();
        console.log('ALL BOOKS : ');
        console.log(response.json());
        for (var i = responseBooks.length - 1; i >= 0; i--) {
          this.dataService.books.push(responseBooks[i]);
        }
      }
    );
  }
}
