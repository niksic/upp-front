import { Injectable } from '@angular/core';
import {Http, RequestOptions, Headers} from '@angular/http';
import {LoginModel} from "./models/login.model";
import {ChangePasswordModel} from "./change-password/change-password.model";
import {CategoryModel} from "./models/category.model";
import {EBooksModel} from "./models/eBooks.model";
import {SimpleQueryModel} from "./models/simple-query.model";
import {AdvancedQueryModel} from "./models/advanced-query.model";

@Injectable()
export class ServerService {

  public loggedUser: LoginModel;

  public isLoggedIn: boolean;

  private  urlBackend = 'http://localhost:8080/api';

  constructor(private http: Http) {

  }

  postForLogin(loginModel: LoginModel) {
    return this.http.post(this.urlBackend + '/login', loginModel);
  }

  putForEditProfile(loginModel: LoginModel) {
    console.log('PUT : ' + this.urlBackend + '/users/' + loginModel.id);
    return this.http.put(this.urlBackend + '/users/' + loginModel.id, loginModel);
  }

  putForChangePassword(changePasswordModel: ChangePasswordModel) {
    console.log('PUT : ' + this.urlBackend + '/users/changePassword/' + this.loggedUser.id);
    return this.http.put(this.urlBackend + '/users/changePassword/' + this.loggedUser.id, changePasswordModel);
  }

  getListOfUsers() {
    console.log('GET: ' + this.urlBackend + '/users');
    return this.http.get(this.urlBackend + '/users');
  }

  getListOfCategories() {
    console.log('GET: ' + this.urlBackend + '/category');
    return this.http.get(this.urlBackend + '/category');
  }

  getUser(id: string) {
    console.log('GET: ' + this.urlBackend + '/users/' + id);
    return this.http.get(this.urlBackend + '/users/' + id);
  }

  getCategory(id: string) {
    console.log('GET: ' + this.urlBackend + '/category/' + id);
    return this.http.get(this.urlBackend + '/category/' + id);
  }

  postUser(user: LoginModel) {
    console.log('POST: ' + this.urlBackend + '/users');
    return this.http.post(this.urlBackend + '/users', user);
  }

  postCategory(category: CategoryModel) {
    console.log('POST: ' + this.urlBackend + '/category');
    return this.http.post(this.urlBackend + '/category', category);
  }

  deleteUser(id: number) {
    console.log('DELETE: ' + this.urlBackend + '/users/' + id);
    return this.http.delete(this.urlBackend + '/users/' + id);
  }

  isAdmin() {
    if (this.isLoggedIn) {
      if (this.loggedUser.type === 'ADMINISTRATOR') {
        return true;
      }
    }
    return false;
  }

  isSubscriber() {
    if (this.isLoggedIn) {
      if (this.loggedUser.type === 'SUBSCRIBER') {
        return true;
      }
    }
    return false;
  }

  putForEditCategory(category: CategoryModel) {
    console.log('PUT : ' + this.urlBackend + '/category/' + category.id);
    return this.http.put(this.urlBackend + '/category/' + category.id, category);
  }


  deleteCategory(id: number) {
    console.log('DELETE: ' + this.urlBackend + '/category/' + id);
    return this.http.delete(this.urlBackend + '/category/' + id);
  }

  getAllBooks() {
    console.log('GET: ' + this.urlBackend + '/ebooks');
    return this.http.get(this.urlBackend + '/ebooks');
  }

  getBooksByCategory(categoryId: number) {
    console.log('GET: ' + this.urlBackend + '/category/' + categoryId + '/ebooks');
    return this.http.get(this.urlBackend + '/category/' + categoryId + '/ebooks');
  }

  getBook(id: string) {
    console.log('GET: ' + this.urlBackend + '/ebooks/' + id);
    return this.http.get(this.urlBackend + '/ebooks/' + id);
  }

  getLanguages() {
    console.log('GET: ' + this.urlBackend + '/languages');
    return this.http.get(this.urlBackend + '/languages');
  }

  getLanguage(id: number) {
    console.log('GET: ' + this.urlBackend + '/languages/' + id);
    return this.http.get(this.urlBackend + '/languages/' + id);
  }

  postBook(book: EBooksModel) {
    console.log('POST: ' + this.urlBackend + '/category/' + book.category.id + '/ebooks');
    return this.http.post(this.urlBackend + '/category/' + book.category.id + '/ebooks', book);
  }

  deleteBook(id: number) {
    console.log('DELETE: ' + this.urlBackend + '/ebooks/' + id);
    return this.http.delete(this.urlBackend + '/ebooks/' + id);
  }

  putForUpdateBook(book: EBooksModel) {
    console.log('PUT: ' + this.urlBackend + '/category/' + book.category.id + '/ebooks/' + book.id);
    return this.http.put(this.urlBackend + '/category/' + book.category.id + '/ebooks/' + book.id, book);
  }

  uploadFile(filedate:any){
    const file: File = filedate;
    const formData: FormData = new FormData();
    formData.append('file', file, file.name);
    const headers = new Headers();
    headers.append('Accept', 'application/json');
    const options = new RequestOptions({ headers: headers });
    return this.http.post(this.urlBackend + '/ebooks/upload', formData, options);
  }

  canDownload(category: string) {
    console.log(this.loggedUser);
    if (this.isLoggedIn) {
      if (this.loggedUser.type === 'ADMINISTRATOR') {
        return true;
      }else if (this.loggedUser.type === 'SUBSCRIBER') {
        if (this.loggedUser.category === 'every') {
          return true;
        }
        if (this.loggedUser.category === category) {
          return true;
        } else {
          console.log('ulogovan al nije to ta categtorija');
          return false;
        }
      }
    }
    return false;
  }

  postSearchedBooks(query: SimpleQueryModel, type: string) {
    console.log('POST: ' + this.urlBackend + '/search/' + type);
    return this.http.post(this.urlBackend + '/search/' + type, query);
  }

  postSearchedBooksBool(query: AdvancedQueryModel, type: string) {
    console.log('POST: ' + this.urlBackend + '/search/' + type);
    return this.http.post(this.urlBackend + '/search/' + type, query);
  }



}
