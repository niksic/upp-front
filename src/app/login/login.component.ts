import { Component, OnInit } from '@angular/core';
import {LoginModel} from '../models/login.model';
import { Response} from '@angular/http';
import {ServerService} from "../server.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  private user: LoginModel;

  constructor(private serverService: ServerService, private router: Router) { }

  ngOnInit() {
    this.user = new LoginModel('', '', '', '', '', '', '');
  }

  onSubmit() {
    console.log(this.user);
    this.serverService.postForLogin(this.user).subscribe(
      (response: Response) => {
        let loginModel: LoginModel = response.json();
        if (loginModel.status === 'VALID') {
          this.serverService.isLoggedIn = true;
          this.serverService.loggedUser = loginModel;
          toastr.success('Login successfully finished!');
          this.router.navigateByUrl('/home');
        } else {
          this.serverService.isLoggedIn = false;
          this.serverService.loggedUser = this.user;
          toastr.error('Login failed!');
        }
      }
    );
  }

}
