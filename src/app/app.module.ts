import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { LoginComponent } from './login/login.component';
import {AppRoutingModule} from './app-routing.module';
import { HomeComponent } from './home/home.component';
import {ServerService} from './server.service';
import { ProfileComponent } from './profil/profile.component';
import { ChangePasswordComponent } from './change-password/change-password.component';
import { UsersComponent } from './administrator/users/users.component';
import { UserItemComponent } from './administrator/users/user-item/user-item.component';
import { UserDetailComponent } from './administrator/users/user-detail/user-detail.component';
import { UserAddComponent } from './administrator/users/user-add/user-add.component';
import {DataService} from "./data.service";
import { CategoriesComponent } from './administrator/categories/categories.component';
import { CategoryItemComponent } from './administrator/categories/category-item/category-item.component';
import { CategoryDetailComponent } from './administrator/categories/category-detail/category-detail.component';
import { CategoryAddComponent } from './administrator/categories/category-add/category-add.component';
import { EBooksComponent } from './administrator/e-books/e-books.component';
import { EBookItemComponent } from './administrator/e-books/e-book-item/e-book-item.component';
import { EBookDetailComponent } from './administrator/e-books/e-book-detail/e-book-detail.component';
import { EBookAddComponent } from './administrator/e-books/e-book-add/e-book-add.component';
import { SearchComponent } from './search/search.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    LoginComponent,
    HomeComponent,
    ProfileComponent,
    ChangePasswordComponent,
    UsersComponent,
    UserItemComponent,
    UserDetailComponent,
    UserAddComponent,
    CategoriesComponent,
    CategoryItemComponent,
    CategoryDetailComponent,
    CategoryAddComponent,
    EBooksComponent,
    EBookItemComponent,
    EBookDetailComponent,
    EBookAddComponent,
    SearchComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    HttpModule
  ],
  providers: [ServerService, DataService],
  bootstrap: [AppComponent]
})
export class AppModule { }
