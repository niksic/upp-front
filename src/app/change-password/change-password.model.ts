export class ChangePasswordModel {

  public currentPassword: string;
  public newPassword: string;
  public repeatPassword: string;

  constructor (currentPassword: string, newPassword: string, repeatPassword: string) {
    this.currentPassword = currentPassword;
    this.newPassword = newPassword;
    this.repeatPassword = repeatPassword;
  }

}
