import { Component, OnInit } from '@angular/core';
import {LoginModel} from "../models/login.model";
import {ServerService} from "../server.service";
import {Router} from "@angular/router";
import {Response} from '@angular/http';
import {ChangePasswordModel} from "./change-password.model";

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.css']
})
export class ChangePasswordComponent implements OnInit {

  public changePasswordModel: ChangePasswordModel = new ChangePasswordModel('', '', '');

  constructor(private serverService: ServerService, private router: Router) {

  }

  ngOnInit() {
  }

  onCancel() {
    this.clearPasswords();
    this.router.navigateByUrl('/profile');
  }

  onChangePassword() {
    if (this.changePasswordModel.currentPassword !== this.serverService.loggedUser.userPassword) {
      this.clearPasswords();
      toastr.error('Current password is invalid!');
      return;
    }
    if (this.changePasswordModel.newPassword !== this.changePasswordModel.repeatPassword) {
      this.clearPasswords();
      toastr.error('New password and repeated password does not match!');
      return;
    }
    this.serverService.putForChangePassword(this.changePasswordModel).subscribe(
      (response: Response) => {
        let loginModel: LoginModel = response.json();
        if (loginModel.status === 'VALID') {
          this.serverService.loggedUser = loginModel;
          toastr.success('Password is changed!');
          this.router.navigateByUrl('/profile');
        } else {
          toastr.error('Password is not changed!');
        }
        this.onCancel();
      }
    );

  }

  clearPasswords() {
    this.changePasswordModel.currentPassword = '';
    this.changePasswordModel.newPassword = '';
    this.changePasswordModel.repeatPassword = '';
  }



}
