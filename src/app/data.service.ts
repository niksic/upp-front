import { Injectable } from '@angular/core';
import {LoginModel} from "./models/login.model";
import {CategoryModel} from "./models/category.model";
import {EBooksModel} from "./models/eBooks.model";
import {LanguageModel} from "./models/language.model";

@Injectable()
export class DataService {

  public users: LoginModel [];
  public categories: CategoryModel [];
  public books: EBooksModel[];
  public languages: LanguageModel[];

  constructor() {
    this.users = [];
    this.categories = [];
    this.books = [];
  }

  updateUser (user: LoginModel) {
    for (var i = this.users.length - 1; i >= 0; i--) {
      if (this.users[i].id === user.id) {
        this.users[i] = user;
      }
    }
  }

  deleteUser (id: number) {
    for (var i = this.users.length - 1; i >= 0; i--) {
      if (this.users[i].id === id) {
        console.log('user is deleted');
        this.users.splice(i, 1);
      }
    }
  }

  updateCategory (category: CategoryModel) {
    for (var i = this.categories.length - 1; i >= 0; i--) {
      if (this.categories[i].id === category.id) {
        this.categories[i] = category;
      }
    }
  }

  deleteCategory (id: number) {
    for (var i = this.categories.length - 1; i >= 0; i--) {
      if (this.categories[i].id === id) {
        console.log('category is deleted');
        this.categories.splice(i, 1);
      }
    }
  }


  deleteBook(id: number) {
    for (var i = this.books.length - 1; i >= 0; i--) {
      if (this.books[i].id === id) {
        console.log('book is deleted');
        this.books.splice(i, 1);
      }
    }
  }

  updateBook(book: EBooksModel) {
    for (var i = this.books.length - 1; i >= 0; i--) {
      if (this.books[i].id === book.id) {
        if (book.category.name === this.books[i].category.name) {
          this.books[i] = book;
        }else {
          this.books.splice(i, 1);
        }
      }
    }
  }
}
