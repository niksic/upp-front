import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {LoginComponent} from './login/login.component';
import {ProfileComponent} from "./profil/profile.component";
import {HomeComponent} from "./home/home.component";
import {ChangePasswordComponent} from "./change-password/change-password.component";
import {UsersComponent} from "./administrator/users/users.component";
import {UserDetailComponent} from "./administrator/users/user-detail/user-detail.component";
import {UserAddComponent} from "./administrator/users/user-add/user-add.component";
import {CategoriesComponent} from "./administrator/categories/categories.component";
import {CategoryAddComponent} from "./administrator/categories/category-add/category-add.component";
import {CategoryDetailComponent} from "./administrator/categories/category-detail/category-detail.component";
import {EBooksComponent} from "./administrator/e-books/e-books.component";
import {EBookAddComponent} from "./administrator/e-books/e-book-add/e-book-add.component";
import {EBookDetailComponent} from "./administrator/e-books/e-book-detail/e-book-detail.component";
import {SearchComponent} from "./search/search.component";

const appRoutes: Routes = [
  { path: 'home', component: HomeComponent },
  { path: 'login', component: LoginComponent },
  { path: 'profile', component: ProfileComponent },
  { path: 'search', component: SearchComponent},
  { path: 'changePassword', component: ChangePasswordComponent },
  { path: 'users', component: UsersComponent, children: [
    { path: 'add', component: UserAddComponent},
    { path: ':id', component: UserDetailComponent},
  ]},
  { path: 'categories', component: CategoriesComponent, children: [
    { path: 'add', component: CategoryAddComponent},
    { path: ':id', component: CategoryDetailComponent},
  ]},
  { path: 'eBooks', component: EBooksComponent, children: [
     { path: 'add', component: EBookAddComponent},
     { path: ':id', component: EBookDetailComponent},
  ]},
];
@NgModule({
  imports: [RouterModule.forRoot(appRoutes)],
  exports: [RouterModule]
})
export class AppRoutingModule {

}
