import { Component, OnInit } from '@angular/core';
import {ServerService} from "../server.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  constructor(private serverService: ServerService, private router: Router) { }

  ngOnInit() {

  }

  onLogout() {
    this.serverService.loggedUser = null;
    this.serverService.isLoggedIn = false;
    this.router.navigateByUrl('/login');
  }

}
