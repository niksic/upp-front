
export class AdvancedQueryModel {

  public field1: string;
  public value1: string;
  public field2: string;
  public value2: string;
  public operation: string;

  constructor(field1: string, value1: string, field2: string, value2: string, operation: string) {
    this.field1 = field1;
    this.value1 = value1;
    this.field2 = field2;
    this.value2 = value2;
    this.operation = operation;
  }

}
