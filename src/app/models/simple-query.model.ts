
export class SimpleQueryModel {

  public field: string;
  public value: string;

  constructor(field: string, value: string) {
    this.field = field;
    this.value = value;
  }

}
