import {CategoryModel} from "./category.model";
import {LanguageModel} from "./language.model";

export class EBooksModel {

  public id: number;
  public title: string;
  public author: string;
  public keywords: string;
  public publicationYear: number;
  public fileName: string;
  public mime: string;
  public category: CategoryModel;
  public language: LanguageModel;
  public downloadString ?: string;
  public highlight ?: string;

  constructor(title: string, author: string, keywords: string, publicationYear: number, fileName: string, mime: string) {
    this.title = title;
    this.author = author;
    this.keywords = keywords;
    this.publicationYear = publicationYear;
    this.fileName = fileName;
    this.mime = mime;
    this.category = new CategoryModel('');
    this.language = new LanguageModel('');
  }
}
