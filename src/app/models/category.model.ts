export class CategoryModel {

  public id?: number;
  public name: string;

  constructor(name: string) {
    this.name = name;
  }

}
