
export class LoginModel {

  public id?: number;
  public firstname: string;
  public lastName: string;
  public username: string;
  public userPassword: string;
  public type: string;
  public status: string;
  public category: string;


  constructor(firstname: string, lastName: string, username: string, userPassword: string, type: string, status: string, category: string) {
    this.firstname = firstname;
    this.lastName = lastName;
    this.username = username;
    this.userPassword = userPassword;
    this.type = type;
    this.status = status;
    this.category = category;
  }

}
