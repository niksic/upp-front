import { Component, OnInit } from '@angular/core';
import {EBooksModel} from "../models/eBooks.model";
import {ServerService} from "../server.service";
import {Response} from "@angular/http";
import {SimpleQueryModel} from "../models/simple-query.model";
import {AdvancedQueryModel} from "../models/advanced-query.model";

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {

  books: EBooksModel [];
  booleanQuery = false;
  searchType: string;
  boolCriteria: string;
  firstType: string;
  secondType: string;
  firstValue: string;
  secondValue: string;
  downloadString: string;

  constructor(private serverService: ServerService) { }

  ngOnInit() {
  }

  onSearch() {
    console.log('SEARCH ' + this.firstValue + ' ' + this.firstType + 'za');
    this.books = [];
    if (this.searchType !== 'boolean') {
      if (this.firstValue !== '' && this.firstType !== '' &&
          this.firstValue !== undefined && this.firstType !== undefined ) {
        const query: SimpleQueryModel = new SimpleQueryModel(this.firstType, this.firstValue);
        this.serverService.postSearchedBooks(query, this.searchType).subscribe(
          (response: Response) => {
            this.books = response.json();
            console.log(this.books);
            for (var i = this.books.length - 1; i >= 0; i--) {
              var arr: String [];
              arr = this.books[i].fileName.split('\\');
              this.books[i].downloadString = 'http://localhost:8080/' + arr[arr.length - 1];
            }
          }
        );
      }else {
        toastr.error('Please fill all fields');
      }

    }else if (this.searchType === 'boolean') {
      if (this.firstType !== '' && this.secondType !== '' && this.firstValue !== '' && this.secondValue !== '' &&
          this.firstType !== undefined && this.secondType !== undefined && this.firstValue !== undefined &&
          this.secondValue !== undefined && this.boolCriteria !== '' && this.boolCriteria !== undefined) {
        console.log('BOOLEAN QUERY');
        const query: AdvancedQueryModel = new AdvancedQueryModel(this.firstType, this.firstValue, this.secondType, this.secondValue, this.boolCriteria);
        this.serverService.postSearchedBooksBool(query, this.searchType).subscribe(
          (response: Response) => {
            this.books = response.json();
            for (var i = this.books.length - 1; i >= 0; i--) {
              var arr: String [];
              arr = this.books[i].fileName.split('\\');
              this.books[i].downloadString = 'http://localhost:8080/' + arr[arr.length - 1];
            }
          }
        );
      }else {
        toastr.error('Please fill all fields');
      }

    }
  }

  changeFieldFirst() {

  }

  changeFieldSecond() {

  }

  changeBoolCriteria() {
    if (this.boolCriteria === 'AND') {
      console.log('AND');
    }else if (this.boolCriteria === 'OR') {
      console.log('OR');
    }
  }

  changeSearchType() {
    console.log(this.searchType);
    if (this.searchType === 'boolean') {
      this.booleanQuery = true;
    } else {
      this.booleanQuery = false;
    }
  }


}
